import React, { Component } from 'react'

export class customButton extends Component {
    constructor(props) {
        super(props)

        this.state = {
           showButton: props.showButton
        }
    }
    toggleButton = () =>{
        this.setState({
            showButton: !this.state.showButton
        })
    }

    render() {
        const { showButton } = this.state
            
        const renderElement =   showButton ? 
            <div>
                <button onClick={this.toggleButton} >CLICK HERE</button>
               <h1> Button is Visible now. </h1>
            </div> 
            :
            <div>
                <button onClick={this.toggleButton} >CLICK HERE</button>
            </div>
            
        return (
            renderElement
        )
    }
}

export default customButton